'use strict';

var $ = require('jquery');

$(document).ready(function () {

    function floatingFilter() {

        $(window).scroll(function() {

            var the_top = $(document).scrollTop(),
                the_element = $('.js-filter'),
                note = $('.js-note'),
                the_element_pos = $('.js-filter-wrap').offset().top;

            if (the_top > the_element_pos) {
                the_element.addClass('fixed');
                note.fadeOut();
            }
            else {
                the_element.removeClass('fixed');
                note.fadeIn();
            }

        });

    }

    if ($('.js-filter-wrap').length) {

        floatingFilter();

    }


});